import pygame
from sprite import SpriteSheet

class Block:
    def __init__(self, image, pos, fenetre):
        self.image = image
        self.pos = pos
        self.fenetre = fenetre

    def affiche(self):
        pass

class Level:
    def __init__(self, fenetre):
        self.fenetre = fenetre
        self.terrain = []

    def add_block(self, ed, pos):
        pass

    def is_in(self, pos):
        pass

    def load(self):
        pass
    
    def write(self):
        pass

class Editor:
    def __init__(self):
        # self.data = ...............
        # self.taille_base = ........
        # self.block_rect = .........
        pass

    def get_image(self):
        pass

    def deplace(self, v):
        pass

    def change_taille(self, v):
        pass

class App:
    def __init__(self):
        pygame.init()
        self.fenetre = pygame.display.set_mode((1202, 623))
        self.clock = pygame.time.Clock()
        self.running = True
        self.fps = 25
        self.uptime = 0

        # Découverte de la classe Block
        # À l'initialisation de la fenêtre, tester block 
        image = pygame.image.load('./assets/test_block.png')
        # block = .............
        # block.affiche()

        # Découverte de la classe Sprite
        # À l'initialisation de la fenêtre, tester block et sprites
        # sprite = SpriteSheet("./assets/terrain.png")
        # image = .............
        # block = .............
        # block.affiche()

        ## Zone de tests ##
        # Instructions... #
        ###################

        # Utilisation des classes Level et Editor 
        # self.level = Level(self.fenetre)
        # self.ed = Editor()

    def on_running(self):
        ecoule = self.clock.tick(self.fps)
        self.uptime += ecoule

    def on_exit(self):
        print("Fin du programme", self.uptime/1000, "secondes")

    def on_event(self, event):
        if event.type == pygame.QUIT:
            self.running = False
            pygame.quit()
        # if event.type == pygame.MOUSEBUTTONDOWN:
            # self.level.add_block(self.ed, event.pos)

    def run(self):
        while self.running:
            self.on_running()
            for event in pygame.event.get():
                self.on_event(event)
        self.on_exit()

A = App()
A.run()
 
