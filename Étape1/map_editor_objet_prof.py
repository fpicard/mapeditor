import pygame
from sprite import SpriteSheet

class Block:
    def __init__(self, image, pos, fenetre):
        self.image = image
        self.pos = pos
        self.fenetre = fenetre

    def affiche(self):
        self.fenetre.blit(self.image, self.pos)
        pygame.display.flip()

class Level:
    def __init__(self, fenetre):
        self.fenetre = fenetre
        self.terrain = []

    def add_block(self, ed, pos):
        block = Block(ed.get_image(), pos, self.fenetre)
        block.affiche()
        self.terrain.append(block)

    def is_in(self, pos):
        pass

    def load(self):
        pass
    
    def write(self):
        pass

class Editor:
    def __init__(self):
        self.data = SpriteSheet("./assets/terrain.png")
        self.base_size = 16
        self.block_rect = [0, 0, self.base_size, self.base_size]

    def get_image(self):
        return self.data.image_at(self.block_rect)

    def move(self, v):
        pass

    def size(self, v):
        pass

class App:
    def __init__(self):
        pygame.init()
        self.fenetre = pygame.display.set_mode((1202, 623))
        self.clock = pygame.time.Clock()
        self.running = True
        self.fps = 25
        self.uptime = 0

        self.level = Level(self.fenetre)
        self.ed = Editor()

        # À l'initialisation de la fenêtre, tester block et sprites
        # sprite = SpriteSheet("./assets/terrain.png")
        # image = sprite.image_at([0,0, 16,16])
        # block = Block(image, (0,0), self.fenetre)
        # block.affiche()

    def on_running(self):
        ecoule = self.clock.tick(self.fps)
        self.uptime += ecoule

    def on_exit(self):
        print("Fin du programme", self.uptime/1000, "secondes")

    def on_event(self, event):
        if event.type == pygame.QUIT:
            self.running = False
            pygame.quit()
        if event.type == pygame.MOUSEBUTTONDOWN:
            self.level.add_block(self.ed, event.pos)

    def run(self):
        while self.running:
            self.on_running()
            for event in pygame.event.get():
                self.on_event(event)
        self.on_exit()

A = App()
A.run()
 
