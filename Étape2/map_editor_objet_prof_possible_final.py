import pygame
import csv
from sprite import SpriteSheet
from random import randint

class Block:
    def __init__(self, image, cat, pos, fenetre):
        self.image = image
        self.cat = cat
        self.pos = pos
        self.fenetre = fenetre

    def affiche(self):
        self.fenetre.blit(self.image, self.pos)
        # pygame.display.flip()

    def is_in(self, rect):
        rect1 = self.image.get_rect()
        rect1.topleft = self.pos
        return rect1.colliderect(rect)

        # size = self.image.get_rect()[2:4]
        # return self.pos[0] <= pos[0] <= self.pos[0] + size[0] and self.pos[1] <= pos[1] <= self.pos[1] + size[1]
            

class Bob(Block):
    def __init__(self, images, pos, fenetre):
        super().__init__(images, 'Bob', pos, fenetre)
        print(self.image)
        self.etat = 1
        self.nframe = 0
        self.frame_number = len(images)

    def update(self, dt, level):
        if self.etat == 1:
            aleax = randint(-10, 10)
            aleay = randint(-10, 10)
            self.pos = (self.pos[0] + aleax, self.pos[1] + aleay)
            self.nframe = (self.nframe + 1)%self.frame_number

            # On teste si on rentre dans un mur
            rect = self.image[self.nframe].get_rect()
            rect.topleft = self.pos
            if level.is_in(rect):
                print('Aïe !')
                self.etat = 0

    def affiche(self):
        trans = self.image[self.nframe].convert_alpha()
        # trans.fill((170, 204, 255))
        self.fenetre.blit(self.image[self.nframe], self.pos)
        # pygame.display.flip()

class Level:
    def __init__(self, fenetre):
        self.fenetre = fenetre
        self.niveau = {'terrain':[], 
                        'ennemis':[],
                        'mario':[]}

    def add_block(self, ed, pos):
        grid_pos = (pos[0] - pos[0]%ed.base_size, pos[1] - pos[1]%ed.base_size)
        block = Block(ed.get_image(), ed.cat, grid_pos, self.fenetre)
        # block.affiche()
        self.niveau[ed.cat].append(block)

    def is_in(self, rect):
        for cat in self.niveau.keys():
            for block in self.niveau[cat]:
                if block.is_in(rect):
                    return block
        return False

    def load(self, filename):
        with open(filename, newline='') as csvfile:
            reader = csv.reader(csvfile, delimiter=';')
            for line in reader:
                # Quoi faire avec la ligne lue
                # Attention : line = str !!
                pass
    
    def write(self, filename):
        with open(filename, 'w', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=';')
            for cat in self.niveau.keys():
                for block in self.niveau[cat]:
                    writer.writerow(['what', 'to', 'write'])

class Editor:
    def __init__(self):
        self.cat = 'terrain'
        self.data = {'terrain':SpriteSheet("./assets/terrain.png"),
                     'ennemis':SpriteSheet("./assets/ennemies.png"),
                     'mario':SpriteSheet("./assets/mario.png")}
        self.base_size = 16
        self.block_rect = [0, 0, self.base_size, self.base_size]

    def get_image(self):
        return self.data[self.cat].image_at(self.block_rect)

    def get_image_at(self, cat, rects):
        return self.data[cat].images_at(rects)

    def select(self, cat):
        self.cat = cat

    def move(self, v1, v2):
        self.block_rect[0] = self.block_rect[0] + v1*self.base_size
        self.block_rect[1] = self.block_rect[1] + v2*self.base_size

    def size(self, v1, v2):
        self.block_rect[2] = self.block_rect[2] + v1*self.base_size
        self.block_rect[3] = self.block_rect[3] + v2*self.base_size


class App:
    def __init__(self):
        pygame.init()
        self.fenetre = pygame.display.set_mode((1202, 623), pygame.DOUBLEBUF)
        self.bg_color = (170, 204, 255)
        self.clock = pygame.time.Clock()
        self.running = True
        self.fps = 10
        self.uptime = 0

        # Initialisation des objets
        self.level = Level(self.fenetre)
        self.ed = Editor()
        # Objet animé
        self.bob = Bob(self.ed.get_image_at('ennemis', [[192, 8, 16, 24], [208, 8, 16, 24]]), (256,256), self.fenetre)
        

    def on_running(self):
        dt = self.clock.tick(self.fps)/1000
        self.uptime += dt

        # Mettre à jour tous les blocs animés.
        self.bob.update(dt, self.level)

        # Dessiner tous les blocs
        self.fenetre.fill(self.bg_color)
        self.bob.affiche()
        self.fenetre.blit(self.ed.get_image(), (512, 0))
        for cat in self.level.niveau.keys():
            for block in self.level.niveau[cat]:
                block.affiche()

        # Mettre à jour l'affichage
        pygame.display.update()

    def on_exit(self):
        print("Fin du programme", round(self.uptime, 2), "secondes")
        # if input("Enrigistrer le fichier") != '':
            # self.level.write('level.csv')
        

    def on_event(self, event):
        if event.type == pygame.QUIT:
            self.running = False
            pygame.quit()
        if event.type == pygame.MOUSEBUTTONDOWN:
            self.level.add_block(self.ed, event.pos)
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_F1:
                self.ed.select('terrain')
            elif event.key == pygame.K_F2:
                self.ed.select('ennemis')
            elif event.key == pygame.K_F3:
                self.ed.select('mario')
            
            elif event.key == pygame.K_LEFT:
                self.ed.move(-1, 0)
            elif event.key == pygame.K_RIGHT:
                self.ed.move(1, 0)
            elif event.key == pygame.K_UP:
                self.ed.move(0, -1)
            elif event.key == pygame.K_DOWN:
                self.ed.move(0, 1)


    def run(self):
        while self.running:
            self.on_running()
            for event in pygame.event.get():
                self.on_event(event)
        self.on_exit()

A = App()
A.run()
 
