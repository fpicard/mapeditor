import pygame

class Champi:
    def __init__(self, pos, fenetre):
        self.image = pygame.image.load("./mushroom.png")
        self.pos = pos
        self.fenetre = fenetre

    def affiche(self):
        self.fenetre.blit(self.image, self.pos)
        pygame.display.flip()

class App:
    def __init__(self):
        pygame.init()
        self.fenetre = pygame.display.set_mode((1202, 623))
        self.fond = pygame.image.load("./background.png")
        self.fenetre.blit(self.fond, (0,0))
        pygame.display.flip()

        self.clock = pygame.time.Clock()
        self.running = True
        self.fps = 25
        self.uptime = 0

    def on_running(self):
        ecoule = self.clock.tick(self.fps)
        self.uptime += ecoule

    def on_exit(self):
        print("Fin du programme", self.uptime/1000, "secondes")

    def on_event(self, event):
        if event.type == pygame.QUIT:
            self.running = False
            pygame.quit()
        if event.type == pygame.MOUSEBUTTONDOWN:
            Champi(event.pos, self.fenetre).affiche()

    def run(self):
        while self.running:
            self.on_running()
            for event in pygame.event.get():
                self.on_event(event)
        self.on_exit()

A = App()
A.run()
 
