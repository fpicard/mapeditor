import pygame

class Champi:
    def __init__(self, pos, fenetre):
        self.image = pygame.image.load("./mushroom.png")
        self.pos = pos
        self.fenetre = fenetre

    def affiche(self):
        pass

class App:
    def __init__(self):
        pygame.init()
        self.fenetre = pygame.display.set_mode((1202, 623))

        # Afficher un fond personnalisé
        # self.fond = 
        # ..............blit(..., ...)
        # pygame.display.flip()

        # Variables d'exécution
        # self.clock   = ...
        # self.running = ...
        # self.fps     = ...
        # self.uptime  = ...

    def on_running(self):
        pass

    def on_exit(self):
        pass

    def on_event(self, event):
        pass

    def run(self):
        while self.running:
            self.on_running()
            for event in pygame.event.get():
                self.on_event(event)
        self.on_exit()

A = App()
A.run()
 
